

from flask import Flask, render_template
from flask.ext.script import Manager, Server
import re
import codecs

class DataDB(object):
    
    def __init__(self):
        
        dataStorage = []
        dataNames = {}
        SumAge = 0
        CountUsers = 0
        AvgAge = 0
        
        with codecs.open("file.txt", "r", encoding='utf-8') as textfile:
            
            for line in textfile.readlines():
                db  = {}
                line = line.rstrip()
                parts = re.split(';', line)
                db['name'] = parts[0] 
                db['surname']=parts[1]
                db['address'] = parts[2]
                db['telephone'] = parts[3]
                db['age'] = parts[4]
                db['sex']= parts[0][-1]
            
                dataStorage.append(db)
                SumAge = SumAge + int(parts[4])
                CountUsers += 1

                # Below i add a new dictionary in order to create separately site user.html
                dataNames[parts[0]+ '_'+ parts[1]] = db # Name_Surname

        if not textfile.closed:
            textfile.close()
        
        self.db = dataStorage # This list conatins user data
        self.userlist = dataNames #This helper dictionary will be use to presents user's data in user.html - contains dataStorage
        
        self.countusers = CountUsers #Total number of users
        self.age = SumAge  #Total sum of users' age
        self.avgage = float(SumAge)/float(CountUsers) #Avarage age of users


app = Flask(__name__)
manager = Manager(app)
manager.add_command("runserver", Server())
server = Server(host="0.0.0.0", port=8080)


@app.route('/')
def index():
    myDB = DataDB()
    
    return render_template('index.html', data_base = myDB.db, SumAge = myDB.age, AvgAge = myDB.avgage, Users = myDB.countusers)

@app.route('/user/<name>')
def user(name):
    
    # Function below capitalization the first letter of name and surname in case of wrong calling ( PIOtr_WODziInski -> Piotr_Wodzinski)
    if name.find('_'):
        part_name = re.split('_',name)
        new_name=(part_name[0].lower()).title()
        new_surname=(part_name[1].lower()).title()
        name=new_name + "_" + new_surname
    
    myDB = DataDB()
    
    if myDB.userlist.has_key(name):
        userData = myDB.userlist[name]
        return render_template('user.html', users = userData)
    else:
        return render_template('nouser.html')

if __name__ == '__main__':
    manager.run()
#    app.run(host='0.0.0.0', debug=True, port=8080)

